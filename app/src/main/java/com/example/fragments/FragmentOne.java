package com.example.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class FragmentOne extends android.support.v7.app.Fragment
      implements FragmentMessageReceiver {
    private FragmentMessageListener mMessageBridge;
    private Button mSendToFragTwo;
    private EditText mStartNum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        mSendToFragTwo = (Button) view.findViewById(R.id.btnSendToFragTwo);
        mStartNum = (EditText) view.findViewById(R.id.etStartNumber);
        mSendToFragTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageBridge.onMessageToFragTwo(mStartNum.getText().toString());
            }
        });
        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMessageBridge = (FragmentMessageListener) activity;

    }

    @Override
    public void onMessage(String message) {
        mStartNum.setText(message);
    }

}
