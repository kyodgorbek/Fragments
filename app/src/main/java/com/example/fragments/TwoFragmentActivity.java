package com.example.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

public class TwoFragmentActivity  extends ActionBarActivity
   implements FragmentMessageListener{

   private FragmentOne mFragOne;
   private FragmentTwo;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_two_fragment);
    mFragOne = (FragmentOne) getSupportFragmentManager().findFragmentById(R.id.fragOne);
    mFragTwo = (FragmentTwo) getSupportFragmentManager().findFragmentById(R.id.fragTwo);

   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu adds items to the action bar if it is present
    getMenuInflater().inflate(R.menu.menu two,menu);

    }
}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
