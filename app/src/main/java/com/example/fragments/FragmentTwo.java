package com.example.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class FragmentTwo extends android.support.v4.app.Fragment
        implements FragmentMessageReceiver {

   private final static String TAG = "FragmentTwo";

   private  FragmentMessageListener mMessageBridge;
   private Button mButton1;
   private Button mButton2;
   private Button mButton3;
   private Button mButton4;

   @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container,
                           Bundle savedInstanceState) {
   View view = inflater.inflate(R.layout.fragment_two, container, false);
   OnButtonClick listener = new onButtonClick();
   mButton1 = (Button) view.findViewById(R.id.button1);
   mButton2 = (Button) view.findViewById(R.id.button2);
   mButton3 = (Button) view.findViewById(R.id.button3);
   mButton4 = (Button) view.findViewById(R.id.button4);
   mButton1.setOnClickListener(listener);
   mButton2.setOnClickListener(listener);
   mButton3.setOnClickListener(listener);
   mButton4.setOnClickListener(listener);
   return view;
   }

   @Override
   public void onAttach(Activity activity) {
    super.onAttach(activity);
       mMessageBridge = (FragmentMessageListener) activity;


   }

  private class onButtonClick implements View.OnClickListener {

      @Override
      public void onMessage(String message) {
      }

      int messageInt = 0;
      try

      {
          messageInt = Integer.parseInt(message);
      }

      catch(
      NumberFormatException ex
      )

      {
          Log.e(TAG, ex.getMessage());
          return;
      }

      mButton1.setText(messageInt+"");
      messageInt++;
      mButton2.setText(messageInt+"");
      messageInt++;
      mButton3.setText(messageInt+"");
      messageInt++;
      mButton4.setText(messageInt+"");

  }

