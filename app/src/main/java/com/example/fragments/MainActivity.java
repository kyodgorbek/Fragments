package com.example.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceHolderFragment()}
                    .commit();
        }
    }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_main, container, false);
       return rootView;
   }
}

  public void buttonClick(View view){
        Intent twoFragmentIntent=new Intent(MainActivity.this TwoFragmentActivity.class);
        MainActivity.this.startActivity(twoFragmentIntent);

        }

       @Override
     public boolean onCreateOptionsMenu(Menu menu){

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, main);
        return true;

        }

       @Override
       public boolean onOptionsItemSelected(MenuItem item){

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
       // as you specify a parent activity in AndroidManifest.xml
        }

